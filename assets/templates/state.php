<?php
	$state = 50;
	$lines = file("../../logs/.state"); // for the security concious (should be everyone!)
	echo '<div class="progress progress-striped active">
		  <div class="bar" style="width: 100%;">PROCESSING</div>
		</div>
		<pre class="pre-scrollable">';
	foreach (array_reverse($lines) as $linen => $line) {
		$line_num = count($lines)-$linen;
	    echo '<b>'.$line_num.'</b> : ' . htmlspecialchars($line);
	}
	echo '</pre>';
?>
