from math import sqrt

"""

Dla kazdego z parametrow:
- cena
- reklamy w sieci spolecznosciowej
- jakosc

mozna wybrac zwiekszenie (+1), wartosc standardowa (0) lub zmniejszenie (-1)

"""

class MarketingStrategy(object):
  def __init__(self, price, socialAds, quality):
    self.price = price
    self.socialAds = socialAds
    self.quality = quality
  
  def getPriceCoeff(self):
    priceCoeffs = {-1: 0.9, 0: 1.0, 1: 1.1}
    return priceCoeffs[self.price]
    
  def getQualityCoeff(self):
    qualityCoeffs = {-1: 0.8, 0: 1.0, 1: 1.05}
    return qualityCoeffs[self.quality]

  @classmethod
  def getQualityCoeffBySpending(cls, spending):
    return 0.8 + sqrt(0.00004*max(0, spending))

  @classmethod
  def getSpendingForQuality(cls, quality):
    return (max(0.0, (quality - 0.8)) ** 2) / 0.00004
    
  def getQualitySpending(self):
    return self.getSpendingForQuality(self.getQualityCoeff())
    
  def __str__(self):
    return "%d, %d, %d" % (self.price, self.socialAds, self.quality)

