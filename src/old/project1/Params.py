from Flight import Flight
from Airline import Airline

CyclesNumber = 5
TicketingTime = 90
DemandMeanBusiness = 95
DemandMeanLeisure = 450
ShapeBusiness = 0.01
ShapeLeisure =  0.035
intensityBusiness = 5
intensityLeisure = 10
DemandSigmaBusiness = 9.75
DemandSigmaLeisure = 21

airline1Capacity = 120
airline2Capacity = 150

def createAirlines(strategyA, strategyB):
  flightsA = [Flight(101,  7*60, 12*60, 150, 60, airline1Capacity),
              Flight(102, 10*60, 15*60, 150, 60, airline1Capacity)]
  flightsB = [Flight(201,  8*60, 13*60, 170, 60, airline2Capacity),
              Flight(202,  9*60, 14*60, 150, 60, airline2Capacity)]

  airlines = [Airline("A", flightsA, strategyA),
              Airline("B", flightsB, strategyB)]
              
  return airlines
              
