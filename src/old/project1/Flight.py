class Flight(object):
  def __init__(self, flightNumber, departureTime, arrivalTime, businessFare, leisureFare, capacity):
    self.flightNumber = flightNumber
    self.departureTime = departureTime
    self.arrivalTime = arrivalTime
    self.businessFare = businessFare
    self.leisureFare = leisureFare
    self.capacity = capacity
    self.ticketsSoldBusiness = [0]*90
    self.ticketsSoldLeisure = [0]*90
    
    self.wantedToBuyBusiness = 0
    self.wantedToBuyLeisure = 0
    
  def isFull(self):
    return self.capacity == self.ticketsSold()

  def availablePlaces(self):
    return self.capacity - self.ticketsSold()

  def reserveTicket(self, isBusiness, day):
    if isBusiness:
      self.ticketsSoldBusiness[day] += 1
      # print("buis" + str(self.ticketsSoldBusiness))
    else:
      self.ticketsSoldLeisure[day] += 1
      # print("leis" + str(self.ticketsSoldLeisure))

  def ticketsSold(self):
    return sum(self.ticketsSoldBusiness) + sum(self.ticketsSoldLeisure)
