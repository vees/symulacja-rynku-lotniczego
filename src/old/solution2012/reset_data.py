"""
Generates (or overwrites) the file json.data with default values of model's parameters.
"""

import json


tab = {
    # business
    "b-fare" : -4.216,
    "b-duration" : -1.227,
    "b-arrive_early" : -0.161,
    "b-arrive_late" : -0.471,
    "b-depart_early" : -0.803,
    "b-depart_late" : -0.522,
    "b-lower_window" : 0.75,
    "b-upper_window" : 0.50,
    "b-intensity": 5,
    "b-shape": 0.01,
    "b-demand_mean": 95,
    "b-demand_sigma": 9.75,
    # leisure
    "l-fare" : -4.549,
    "l-duration" : -0.540,
    "l-arrive_early" : -0.092,
    "l-arrive_late" : -0.449,
    "l-depart_early" : -0.364,
    "l-depart_late" : -0.094,
    "l-lower_window" : 1.50,
    "l-upper_window" : 1.25,
    "l-intensity": 10,
    "l-shape": 0.035,
    "l-demand_mean": 450,
    "l-demand_sigma": 21
}

f = open("json.data", "w")
f.write(json.dumps(tab))
f.close()
