class Airline(object):
  def __init__(self, name, flights, strategy):
    self.name = name
    self.flights = flights
    self.strategy = strategy
    
  def initialize(self, marketingStrategy):
    self.revenue = 0
    self.setMarketingStrategy(marketingStrategy)
    
  def setMarketingStrategy(self, marketingStrategy):
    self.marketingStrategy = marketingStrategy

  def newCycle(self):
    for flight in self.flights:
      flight.resetForNewCycle()
      
  def endCycle(self):
    costs = 0
    for flight in self.flights:
      qualityCost = self.marketingStrategy.getQualitySpending()
      costs += qualityCost

    self.revenue -= costs

  def canBuy(self, flight, isBusiness):
    return self.strategy.canBuy(flight, isBusiness)
    
  def buyTicket(self, offer, isBusiness, day):
    flight = offer["flight"]
    if isBusiness:
      flight.wantedToBuyBusiness += 1
    else:
      flight.wantedToBuyLeisure += 1
    if self.canBuy(flight, isBusiness):
      self.revenue += offer["fare"]
      flight.reserveTicket(isBusiness, day)
      return True
    else:
      return False
      
  def makeOffer(self, flight, isBusiness):
    return {"fare": flight.getFare(isBusiness) * self.marketingStrategy.getPriceCoeff(),
            "arrivalTime": flight.arrivalTime,
            "departureTime": flight.departureTime,
            "quality": self.marketingStrategy.getQualityCoeff(),
            "airline": self.name,
            "flight": flight}
            
  def getOffers(self, isBusiness):
    return [self.makeOffer(flight, isBusiness) for flight in self.flights]
    
